﻿using System;
using BussinessLogicWT;

namespace ConsoleAppWT
{
    class Program
    {
        //USD wallet
        static USDWallet UsdWallet;
        //EUR wallet
        static EURWallet EurWallet;
        //total number of spend operations
        static int CountOfAllSpendOperations;
        //total spend money
        static decimal TotalSpendSum;
        static void Main(string[] args)
        {
            var whileResult = true;

            do
            {
                Console.WriteLine();
                Console.WriteLine("***********************************************************************");
                Console.WriteLine("Select wallet to work: 1 - USD, 2 - EUR");
                var walletType = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Select action: 1 - Put money, 2 - Spend money");
                var actionValue = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Sum of operation:");
                var operationSum = Convert.ToInt32(Console.ReadLine());

                //wallet updates
                object resusltOfUpdate;
                if (walletType == 1 && UsdWallet == null)
                {
                    UsdWallet = new USDWallet();
                    resusltOfUpdate = CurrencySelector.SelectCurrency(UsdWallet, actionValue, operationSum);
                }
                if (walletType == 1 && UsdWallet != null)
                {
                    resusltOfUpdate = CurrencySelector.SelectCurrency(UsdWallet, actionValue, operationSum);
                }
                if (walletType == 2 && EurWallet == null)
                {
                    EurWallet = new EURWallet();
                    resusltOfUpdate = CurrencySelector.SelectCurrency(EurWallet, actionValue, operationSum);
                }
                if (walletType == 2 && EurWallet != null)
                {
                    resusltOfUpdate = CurrencySelector.SelectCurrency(EurWallet, actionValue, operationSum);
                }

                //update total spend money parameter and count of operations
                if (actionValue == 2)
                {                   
                    TotalSpendSum = walletType == 1 ? TotalSpendSum + operationSum : TotalSpendSum + Convert.ToDecimal(operationSum * 1.12);
                    CountOfAllSpendOperations++;
                }

                //update Bonus wallet
                bool updateBonusWallet;
                if (actionValue == 2 && operationSum >= 5)
                {
                    updateBonusWallet = BonusWallet.UpdateBonusWallet(walletType, Convert.ToDecimal(operationSum * 0.01));
                }
                if (actionValue == 2 && CountOfAllSpendOperations >= 10)
                {
                    updateBonusWallet = BonusWallet.UpdateBonusWallet(walletType, TotalSpendSum * Convert.ToDecimal(0.01));
                }
                Console.WriteLine("***********************************************************************");
                Console.WriteLine("Summary: Count of spend operation - " + CountOfAllSpendOperations +
                    " total spend money in USD: " + TotalSpendSum);
                Console.WriteLine("***********************************************************************");
                Console.WriteLine();
                Console.WriteLine("***********************************************************************");
                Console.WriteLine("Select action: 1 - Continue operation with currency;");
                Console.WriteLine("Select action: 2 - Show bonus wallet balace;");
                Console.WriteLine("Select action: 3 - Exit from application;");
                var lastChoice =  Console.ReadLine();
                if (Convert.ToInt32(lastChoice) == 2)
                {
                    Console.WriteLine();
                    Console.WriteLine("Bonus wallet balance: " + BonusWallet.SumOfMoney + " (Wallet  currency - USD)");
                    Console.WriteLine("***********************************************************************");
                    Console.WriteLine();
                }
                //exit fron application
                if (Convert.ToInt32(lastChoice) == 3)
                {
                    whileResult = false;
                }
                
            } while (whileResult);
            
        }       
    }
}
