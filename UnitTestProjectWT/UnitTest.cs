﻿using System;
using BussinessLogicWT;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProjectWT
{
    [TestClass]
    public class UnitTest
    {
        //test for USD in sace if put money to wallet
        [TestMethod]
        [DataRow(1, 20)]
        public void USDUpdatorPut(int actionValue, int operationSum)
        {
            var result = USDWallet.USDUpdator(actionValue, operationSum);
            Assert.IsTrue((bool)result, $"Failed to update USD wallet");
        }

        //test for USD in sace if spend money from wallet
        [TestMethod]
        [DataRow(2, 5)]
        public void USDUpdatorSpend(int actionValue, int operationSum)
        {
            var result = USDWallet.USDUpdator(actionValue, operationSum);
            Assert.IsTrue((bool)result, $"Failed to update USD wallet");
        }

        //test for EUR in sace if put money to wallet
        [TestMethod]
        [DataRow(1, 20)]
        public void EURUpdatorPut(int actionValue, int operationSum)
        {
            var result = EURWallet.EURUpdator(actionValue, operationSum);
            Assert.IsTrue((bool)result, $"Failed to update EUR wallet");
        }

        //test for EUR in sace if spend money from wallet
        [TestMethod]
        [DataRow(2, 5)]
        public void EURUpdatorSpend(int actionValue, int operationSum)
        {
            var result = EURWallet.EURUpdator(actionValue, operationSum);
            Assert.IsTrue((bool)result, $"Failed to update EUR wallet");
        }

        //test for Bonus wallet in case if operation held over the USD
        [TestMethod]
        [DataRow(1, 5)]
        public void BonusUpdatorUSD(int walletType, int operationSum)
        {
           var updateBonusWallet = BonusWallet.UpdateBonusWallet(walletType, Convert.ToDecimal(operationSum * 0.01));
            Assert.IsTrue(updateBonusWallet, $"Failed to update Bonus wallet");
        }

        //test for Bonus wallet in case if operation held over the EUR
        [TestMethod]
        [DataRow(2, 5)]
        public void BonusUpdatorEUR(int walletType, int operationSum)
        {
            var updateBonusWallet = BonusWallet.UpdateBonusWallet(walletType, Convert.ToDecimal(operationSum * 0.01));
            Assert.IsTrue(updateBonusWallet, $"Failed to update Bonus wallet");
        }
    }
}
