﻿using System;

namespace BussinessLogicWT
{
    public class Wallet
    {
        public string WalletType { get; set; }
        public int SumOfMoney { get; set; }
        public int CountOfOperation { get; set; }
    }
}
