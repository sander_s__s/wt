﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BussinessLogicWT
{
    public class BonusWallet
    {
        public static decimal SumOfMoney { get; set; }

        /// <summary>
        /// Update Bonus wallet 
        /// </summary>
        /// <param name="wallettype">Type of wallet, USD or EUR.</param>
        /// <param name="operationSum">Sum of opeation.</param>
        /// <returns>Returne tru in case if operation complite success.</returns>
        public static bool UpdateBonusWallet(int wallettype, decimal oparationSum )
        {
            //In case if operation perform with EUR, nead to convert to USD
            SumOfMoney = wallettype == 1 ? SumOfMoney + oparationSum : SumOfMoney + (oparationSum * Convert.ToDecimal(1.12));
            return true;
        }
    }
}
