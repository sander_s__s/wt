﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BussinessLogicWT
{
    public class EURWallet
    {
        //Wallet type.
        public static string WalletType { get; set; }
        //Current money sum.
        public static int SumOfMoney { get; set; }
        //Number of operation with this wallet.
        public static int CountOfOperation { get; set; }

        public EURWallet()
        {
            WalletType = "EUR Wallet";
        }

        /// <summary>
        /// Update EUR wallet 
        /// </summary>
        /// <param name="actionValue">Actions: put or spend money.</param>
        /// <param name="operationSum">Sum of opeation.</param>
        /// <returns>Returne tru in case if operation complited successfully.</returns>
        public static object EURUpdator(int actionValue, int operationSum)
        {
            try
            {
                SumOfMoney = actionValue == 1 ? SumOfMoney + operationSum : SumOfMoney - operationSum;
                CountOfOperation++;
                return true;
            }
            catch
            {
                return false;
            }
           
        }
    }
}
