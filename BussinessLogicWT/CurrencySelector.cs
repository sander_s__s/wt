﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BussinessLogicWT
{
    public class CurrencySelector
    {
        //Select method for each of currencies
        public static object SelectCurrency(object walletCurrency, int actionValue, int operationSum)
        {
            var usd = new USDWallet();

            if (walletCurrency.GetType() == usd.GetType())
            {
                return USDWallet.USDUpdator(actionValue, operationSum);
            }

            return EURWallet.EURUpdator(actionValue, operationSum);
        }
    }
}
