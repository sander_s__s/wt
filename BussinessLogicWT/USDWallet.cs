﻿using System;

namespace BussinessLogicWT
{
    public class USDWallet
    {
        //Wallet type.
        public static string WalletType { get; set; }
        //Current money sum.
        public static int SumOfMoney { get; set; }
        //Number of operation with this wallet.
        public static int CountOfOperation { get; set; }

        public USDWallet()
        {
            WalletType = "USD Wallet";
        }

        /// <summary>
        /// Update USD wallet 
        /// </summary>
        /// <param name="actionValue">Actions: put or spend money.</param>
        /// <param name="operationSum">Sum of opeation.</param>
        /// <returns>Returne tru in case if operation complited successfully.</returns>
        public static object USDUpdator(int actionValue, int operationSum)
        {
            try
            {
                SumOfMoney = actionValue == 1 ? SumOfMoney + operationSum : SumOfMoney - operationSum;
                CountOfOperation++;
                return true;
            }
            catch
            {
                return false;
            }         
        }
    }
}
